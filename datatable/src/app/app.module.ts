import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ReportsComponent } from './components/reports/reports.component';
import { AppRoutingModule } from './app-routing.module';

import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SiteDaysCoverComponent } from './components/site-days-cover/site-days-cover.component';
import { StockLevelMatrixComponent } from './components/stock-level-matrix/stock-level-matrix.component';
import { NumericDistributionComponent } from './components/numeric-distribution/numeric-distribution.component';
import { CustomTooltipComponent } from './components/custom-tooltip/custom-tooltip.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ReportsComponent,
    SiteDaysCoverComponent,
    StockLevelMatrixComponent,
    NumericDistributionComponent,
    CustomTooltipComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    AgGridModule.withComponents([CustomTooltipComponent])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
