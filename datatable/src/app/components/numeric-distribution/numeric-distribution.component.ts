import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { CustomTooltipComponent } from '../custom-tooltip/custom-tooltip.component';

@Component({
  selector: 'app-numeric-distribution',
  templateUrl: './numeric-distribution.component.html',
  styleUrls: ['./numeric-distribution.component.css']
})
export class NumericDistributionComponent implements OnInit {

  public searchValue;
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public sortingOrder;
  public NoOfStores;
  public tooltipShowDelay;
  public frameworkComponents;
  data: any[] = [];

  constructor(private http: HttpClient) {

   }

   //Columns
   Numeric(){
    this.columnDefs = [
      {
        headerName: 'Province',
        field: 'Province',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'StoreName',
        field: 'StoreName',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'Profile',
        field: 'Profile',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'Size',
        field: 'Size',
        width: 100,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'ArticleDescription',
        field: 'ArticleDescription',
        width: 300,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SiteArticleStatus',
        field: 'SiteArticleStatus',
        width: 60,
        sortingOrder: ["asc", "desc"],
        filter:true,
        resizable: true,
        sortable: true,
        tooltipField: 'SiteArticleStatus',
        tooltipComponent: 'customTooltip',
        tooltipComponentParams: { color: '#FFFEE3' },
        valueParser: numberParser,
        cellStyle: cellStyle,
      },
  ];

  this.sortingOrder=["desc", "asc", null];

  this.tooltipShowDelay = 0;

  this.frameworkComponents = { customTooltip: CustomTooltipComponent };

  }

  ngOnInit() {
    //call function on page load
    this.Numeric();
  }

  //pull data from api and populate the grid
   onGridReady(params){
    this.gridApi= params.api;
    this.gridColumnApi = params.columnApi;
    this.http
    // .get("http://localhost/api2/api.php")
    // .subscribe(data => {
    //   console.log(data);
    //   params.api.setRowData(data);
    // })
    .get("https://marketforce.co.za/qsapi/PNPZEROSTOCK.php?cid=1085&channel=2")
    .toPromise().then(res => {
      this.data = res['STOCKOUT'];
      params.api.setRowData(this.data);
      })
 }

 //function to export reports
   onBtExport(){
     var params = {

     };
     // export to csv
     this.gridApi.exportDataAsCsv(params)
   }

   // function for a filtering all columns at once
   quickSearch() {
     this.gridApi.setQuickFilter(this.searchValue);
   }

   //get number of rows displayed which is the number of stores
   numberofStores() {
    this.NoOfStores = this.gridApi.getDisplayedRowCount();
   }
}

// styling each cell depending on the condition
function cellStyle(params) {
  var color = numberToColor(params.value);
  return { backgroundColor: color };
}

// change number to correspond colors

//remove the last ones on 2 fist if and else 0 and 4
function numberToColor(val) {
  if (val === '72' || val === '07' || val === '71') {
    return 'red; color: red';
  } else if (val === '04' || val === '02' || val==='05') {
    return 'green; color: green';
  } else if (val === '61' || val === '62') {
    return '#EABC07';
  }
}

function numberParser(params) {
  var newValue = params.newValue;
  var valueAsNumber;
  if (newValue === null || newValue === undefined || newValue === '') {
    valueAsNumber = null;
  } else {
    valueAsNumber = parseFloat(params.newValue);
  }
  return valueAsNumber;
}
