import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteDaysCoverComponent } from './site-days-cover.component';

describe('SiteDaysCoverComponent', () => {
  let component: SiteDaysCoverComponent;
  let fixture: ComponentFixture<SiteDaysCoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteDaysCoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteDaysCoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
