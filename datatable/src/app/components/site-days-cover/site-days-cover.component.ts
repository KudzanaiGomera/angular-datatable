import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-site-days-cover',
  templateUrl: './site-days-cover.component.html',
  styleUrls: ['./site-days-cover.component.css']
})
export class SiteDaysCoverComponent implements OnInit {

  public searchValue;
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public sortingOrder;
  chosenReport: string = "";
  data: any[] = [];

  constructor(private http: HttpClient) {

   }

   //Store Columns
   Store(){
    this.columnDefs = [
      {
        headerName: 'Province',
        field: 'Province',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'Banner',
        field: 'Banner',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'Profile',
        field: 'Profile',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'Size',
        field: 'Size',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'StoreName',
        field: 'StoreName',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'StoreID',
        field: 'StoreID',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'ArticleDescription',
        field: 'ArticleDescription',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'ArticleNumber',
        field: 'ArticleNumber',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DROS_QTY',
        field: 'DROS_QTY',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysCover',
        field: 'DaysCover',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SOH',
        field: 'SOH',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SiteArticleStatus',
        field: 'SiteArticleStatus',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SiteArticleStatusDesc',
        field: 'SiteArticleStatusDesc',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SourceOfSupply',
        field: 'SourceOfSupply',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'LastSold',
        field: 'LastSold',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'LastOrdered',
        field: 'LastOrdered',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'LastReceived',
        field: 'LastReceived',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysSince_LastSold',
        field: 'DaysSince_LastSold',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysSince_LastOrdered',
        field: 'DaysSince_LastOrdered',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysOrderToReceived',
        field: 'DaysOrderToReceived',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },

  ];
  this.sortingOrder=["desc", "asc", null];
   }

   //DC Columns
   DC(){
    this.columnDefs = [
      {
        headerName: 'Province',
        field: 'Province',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'Banner',
        field: 'Banner',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'StoreName',
        field: 'StoreName',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'StoreID',
        field: 'StoreID',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'ArticleDescription',
        field: 'ArticleDescription',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'ArticleNumber',
        field: 'ArticleNumber',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DROS_QTY',
        field: 'DROS_QTY',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysCover',
        field: 'DaysCover',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SOH',
        field: 'SOH',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SiteArticleStatus',
        field: 'SiteArticleStatus',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SiteArticleStatusDesc',
        field: 'SiteArticleStatusDesc',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SourceOfSupply',
        field: 'SourceOfSupply',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'LastIssued',
        field: 'LastIssued',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'LastOrdered',
        field: 'LastOrdered',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'LastReceived',
        field: 'LastReceived',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysSince_LastIssued',
        field: 'DaysSince_LastIssued',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysSince_LastOrdered',
        field: 'DaysSince_LastOrdered',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysOrderToReceived',
        field: 'DaysOrderToReceived',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },

  ];
  this.sortingOrder=["desc", "asc", null];
   }


  //selecting the report to display
   report(){
    switch(this.chosenReport) {
      case "DC":
        this.onBtDCColumns();
        break;
      case "Store":
        this.onBtStoreColumns();
        break;
      default:
        console.log("Select Report");
    }
  }

   ngOnInit() {
    // call function on page load
    this.report();
  }

  //adding columns for the Store table
  onBtStoreColumns() {
    this.gridApi.setColumnDefs(this.Store());
  }


  //removing some of Store Columns and adding DC
  onBtDCColumns() {
    this.gridApi.setColumnDefs(this.DC());
  }


  // pull data from api and populate the grid
  onGridReady(params){
    this.gridApi= params.api;
    this.gridColumnApi = params.columnApi;
    this.http
    .get("https://marketforce.co.za/qsapi/PNPZEROSTOCK.php?cid=1093")
    .toPromise().then(res => {
      this.data = res['STOCKOUT'];
      params.api.setRowData(this.data);
      })
 }

 //function to export reports
   onBtExport(){
     var params = {

     };
     // export to csv
     this.gridApi.exportDataAsCsv(params)
   }

   // function for a filtering all columns at once
   quickSearch() {
     this.gridApi.setQuickFilter(this.searchValue);
   }


}
