import { Component} from '@angular/core';
import { ITooltipAngularComp } from 'ag-grid-angular';
import { ITooltipParams } from 'ag-grid-community';

@Component({
  selector: 'app-custom-tooltip',
  templateUrl: './custom-tooltip.component.html',
  styleUrls: ['./custom-tooltip.component.css']
})
export class CustomTooltipComponent implements ITooltipAngularComp {
  public params: { color: string } & ITooltipParams;
  public data = {
    ArticleDescription: null,
    ArticleNumber: null,
    SiteArticleStatus: null,
    SiteArticleStatusDesc: null,
    SOH: null,
    DaysCover: null,
    LastSold: null,
    LastReceived: null,
    LastOrdered: null,
  }
  public color: string;

  constructor() { }

  agInit(params: { color: string } & ITooltipParams): void {
    this.params = params;

    this.data = params.api.getDisplayedRowAtIndex(params.rowIndex).data;
    this.color = this.params.color || 'white';
  }
  refresh(){
    return false;
  }

}
