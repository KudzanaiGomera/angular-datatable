import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  public searchValue;
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public sortingOrder;


  data: any[] = [];

  constructor(private http: HttpClient) {

   }

   ngOnInit() {
    this.columnDefs = [

        {
          headerName: 'SupplierID',
          field: 'supplierID',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'Province',
          field: 'province',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'Banner',
          field: 'banner',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'StoreName',
          field: 'storeName',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'StoreID',
          field: 'storeID',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'ArticleDescription',
          field: 'articleDescription',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'ArticleNumber',
          field: 'articleNumber',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'IsApproximate',
          field: 'isApproximate',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'RateOfSaleChannel',
          field: 'rateOfSaleChannel',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysCoverChannel',
          field: 'daysCoverChannel',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'RateOfSale6Wk',
          field: 'rateOfSale6Wk',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysCover6WkRos',
          field: 'daysCover6WkRos',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'RateOfSale21Day',
          field: 'rateOfSale21Day',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysCover21DayRos',
          field: 'daysCover21DayRos',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'RateOfSale7Day',
          field: 'rateOfSale7Day',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysCover7DayRos',
          field: 'daysCover7DayRos',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'StockOnHand',
          field: 'stockOnHand',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'ListingStatus',
          field: 'listingStatus',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'ListingStatusCode',
          field: 'listingStatusCode',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'ListingStatusDesc',
          field: 'listingStatusDesc',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'LastSold',
          field: 'lastSold',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'LastReceived',
          field: 'lastReceived',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'LastOrdered',
          field: 'lastOrdered',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysSince_lastSold',
          field: 'daysSince_lastSold',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysSince_lastOrdered',
          field: 'daysSince_lastOrdered',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysSince_lastReceived',
          field: 'daysSince_lastReceived',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'Days_OrderToReceived',
          field: 'days_OrderToReceived',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
    ];
    this.sortingOrder=["desc", "asc", null];
  }

   onGridReady(params){
    this.gridApi= params.api;
    this.gridColumnApi = params.columnApi;
    this.http
    .get("https://marketforce.co.za/V2/v2api/SDC.php?sid=1077&channel=5&locationType=STORES&clearcache=0")
    .toPromise().then(res => {
      //   console.log(res['SDC'])
        this.data = res['SDC'];
        // console.log(this.data)
        params.api.setRowData(this.data);
      })
    // .subscribe(data => {
    //   console.log(data);
    //   params.api.setRowData(data);
    // })
 }

   onBtExport(){
     var params = {

     };
     // export to csv
     this.gridApi.exportDataAsCsv(params)
   }

   quickSearch() {
     this.gridApi.setQuickFilter(this.searchValue);
   }

}
