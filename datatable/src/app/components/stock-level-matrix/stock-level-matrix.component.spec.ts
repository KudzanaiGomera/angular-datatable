import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockLevelMatrixComponent } from './stock-level-matrix.component';

describe('StockLevelMatrixComponent', () => {
  let component: StockLevelMatrixComponent;
  let fixture: ComponentFixture<StockLevelMatrixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockLevelMatrixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockLevelMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
