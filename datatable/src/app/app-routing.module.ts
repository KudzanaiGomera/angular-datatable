import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NumericDistributionComponent } from './components/numeric-distribution/numeric-distribution.component';
import { ReportsComponent } from './components/reports/reports.component';
import { SiteDaysCoverComponent } from './components/site-days-cover/site-days-cover.component';
import { StockLevelMatrixComponent } from './components/stock-level-matrix/stock-level-matrix.component';

const appRoutes:Routes = [

  {
    path: '',
    component: SiteDaysCoverComponent
  },

  {
    path: 'stock',
    component: StockLevelMatrixComponent
  },

  {
    path: 'status',
    component: ReportsComponent
  },

  {
    path: 'numeric',
    component: NumericDistributionComponent
  }

]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(appRoutes),
  ]
})
export class AppRoutingModule { }
