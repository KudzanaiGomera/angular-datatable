# README #

* is built using angular 11
* make sure you have node js installed

### What is this repository for? ###

* This repo contains a angular 11 ag-grid datatable
* angular 11

### How do I get set up? ###

* git clone git clone https://KudzanaiGomera@bitbucket.org/KudzanaiGomera/angular-datatable.git
* cd angular_datatable
* cd datatable

### How to run? ###
* ng serve

### Contribution guidelines ###

* Writing tests -> Kudzanai Gomera
* Code review -> Junior and Sipho

### Who do I talk to? ###

* Kudzanai Gomera
* Katanga Dev Team