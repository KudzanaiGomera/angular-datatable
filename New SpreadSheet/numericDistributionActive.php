<?php

/**
 * Include Report Generator
 * ARTICLE MATRIX ACTIVE
 */


$start_time = microtime(true);

echo "\n\n".date('Y-m-d H:i:s')." | Generating Store-Article Matrix for Numeric Distribution Sheet - ACTIVE ARTICLES";

/**
 * Configs and setup
 */

// PHP File_get_Contents params
$config_fgc = stream_context_create(array(
    'http' => array(
        'timeout' => 3600
        )
    )
); 

// @NOTE @TODO ADD CELL MAPPING for better management and updates.
// Cell Position Map
$cell_Header_ClientName    = 'B3';
$cell_Header_ChannelName   = 'B4';
$cell_Header_DataDate      = 'B5';
$cell_Header_CreateDate    = 'B6';
$cell_Header_TotalStores   = 'B7';
$cell_Header_TotalArticles = 'B8';
$cell_Header_VendorID_List = 'B9';

$cell_S1_matrix_article_header_row             = 13;
$cell_S1_StoreArticleMartix_ArticlesHeader_col = 'C'; // Starting COL for Article NAme Header

$max_col_num_matrix = 0;


$styleSubHeaderArray =   array(
    'font'  => array(
        'bold'  =>  true,
        'size'  =>  11
    )
);
// SET Matrix Borders (readability)


$styleMatrixBordersArray = [ 
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,                    
        ]       
      
    ],
];



// @TODO - Update to pull status from DB
$status_active_arr = array('1','01','2','02','4','04','5','05');

$status_temp_arr   = array("06",
                            "6",
                            "03",
                            "3",
                            "21",	
                            "60",	
                            "61",	
                            "62",	
                            "70",	
                            "73"
                        );

$ndgap_table_start_row = 13;
$ndgap_table_start_col = 'C';

$spreadsheet->setActiveSheetIndex($sheetid);  // First Sheet** @NOTE Will need to move to 2 when "DASH/OVERVIEW" Sheet created
$sheet = $spreadsheet->getActiveSheet();
$sheet->setTitle('Numeric Distr-GAP (ACTIVE)');


echo "\n<br /> date('Y-m-d H:i:s') - Fetching Data From API ";

// Get DATA
$data_json = file_get_contents("https://marketforce.co.za/qsapi/PNPSTOREARTICLEMATRIX.php?cid=".$client_id."&filter=ACTIVE&format=JSON");
// Json To array
$data_array = json_decode($data_json, TRUE);

//print_r($data_array);
$cnt_active         = 0;  // INFO COUNTERS
$cnt_inactive       = 0;  // INFO COUNTERS
$stores_count       = 0;
$articles_count     = 0;
$stores_arr         = null;
$article_found      = '';
$articlesID_arr     = null;
$articlesID_tmp_arr = null;
$articles_ref_arr   = null;
$matrix             = null;

// STYLE ARRAYS
// 1 LINE EXAMPLE! $objPHPExcel->getActiveSheet()->getStyle('A2:A2')->applyFromArray(array('font' => array('size' => 24,'bold' => true,'color' => array('rgb' => 'eeeeee'))));

// Sheet Sub Header Style

// SET AUTO WIDTH ON COL A and B
$sheet->getColumnDimension( 'A' )->setAutoSize(TRUE);
$sheet->getColumnDimension( 'B' )->setAutoSize(TRUE);

// Write Sheet Headers
$sheet->mergeCells('A2:R2');
$sheet->getStyle('A2')->getFont()->setSize(12);
$sheet->getStyle('A2')->getFont()->setBold(TRUE);
$sheet->setCellValue( 'A2', 'Numeric Distribution / GAP Analysis (ACTIVE)');



$sheet->setCellValue( 'A4', 'SUPPLIER' );
$sheet->getStyle( 'A4')->getFont()->setBold(TRUE);
$sheet->setCellValue( 'B4', $client_info['client_name'] );    // HEADING Client Name

$sheet->setCellValue( 'A5', 'CHANNEL' );
$sheet->getStyle( 'A5')->getFont()->setBold(TRUE);
$sheet->setCellValue( 'B5', $channel );   // HEADING Channel Name  

$sheet->setCellValue( 'A6', 'REPORT DATE' );
$sheet->getStyle( 'A6')->getFont()->setBold(TRUE);
$sheet->setCellValue( 'B6', $data_array['INFO']['DataDate']);  // HEADING Report Data Date
$sheet->getStyle('B6')->getAlignment()->setHorizontal('left');


$sheet->setCellValue( 'A7', 'CREATED DATE' );
$sheet->getStyle( 'A7')->getFont()->setBold(TRUE);
$sheet->setCellValue( 'B7', $datetime);  // HEADING Report Create Date
$sheet->getStyle('B7')->getAlignment()->setHorizontal('left');


$sheet->setCellValue( 'A8', 'STORES' );
$sheet->getStyle( 'A8')->getFont()->setBold(TRUE);


$sheet->setCellValue( 'A9', 'ARTICLES' );
$sheet->getStyle( 'A9')->getFont()->setBold(TRUE);

$sheet->setCellValue( 'A10', 'VENDOR ID' );
$sheet->getStyle( 'A10')->getFont()->setBold(TRUE);
$sheet->getStyle('A10')->getAlignment()->setVertical('top');


//$sheet->setCellValue( 'B8', $data_array['INFO']['Records']);  // HEADING Report Create Date
//$sheet->getStyle('B8')->getAlignment()->setHorizontal('left'); 

// VendorID in SCD (active/viewable)
$vendorIDList = implode("\r", $data_array['INFO']['VendorID']);
$sheet->setCellValue( 'B10', $vendorIDList); 
$sheet->getStyle('B10')->getAlignment()->setWrapText(true);
$sheet->getStyle('B10')->getAlignment()->setHorizontal('left');

// Get Distinct Articles ( for col headers )
// Write "article" heading to sheet -> START at cell C12
$storesID_arr   = array_values(array_unique(array_column($data_array['STOREARTICLEMATRIX'], 'Store_ID')));
$stores_arr     = array_values(array_unique(array_column($data_array['STOREARTICLEMATRIX'], 'Store_Name')));
$stores_count   = count($stores_arr);
$articlesID_tmp_arr = array_column($data_array['STOREARTICLEMATRIX'], 'Store_Article_Number', 'Store_Article_Desc');
ksort($articlesID_tmp_arr,2); // Sort on KEY
$articlesID_arr = array_values(array_unique($articlesID_tmp_arr));
$articles_count = count($articlesID_arr);
$articles_ref_arr   = array_unique(array_column($data_array['STOREARTICLEMATRIX'], 'Store_Article_Desc','Store_Article_Number')); // Reference Article Number to Description


// SET COUNT VALUES IN HEADER SECTION
$sheet->setCellValue( 'B8', $stores_count);  // HEADING Total stores Count Value
$sheet->getStyle('B8')->getAlignment()->setHorizontal('left'); 

$sheet->setCellValue( 'B9', $articles_count);  // HEADING Total Articles Count Value
$sheet->getStyle('B9')->getAlignment()->setHorizontal('left'); 


/*
// "COL 2" Info Headers
$sheet->setCellValue( 'C4', 'LISTING SLOT POTENTIAL' );
$sheet->getStyle( 'C4')->getFont()->setBold(TRUE);
$sheet->mergeCells('C4:I4');

$sheet->setCellValue( 'J4', $stores_count * $articles_count ); 
$sheet->mergeCells('J4:N4');        
$sheet->getStyle('J4')->getAlignment()->setHorizontal('left');


$sheet->setCellValue( 'C5', 'ACTIVE' );
$sheet->getStyle( 'C5')->getFont()->setBold(TRUE);
$sheet->mergeCells('C5:I5');

$sheet->setCellValue( 'C6', 'DELIST/SUSP/INACTIVE' );
$sheet->getStyle( 'C6')->getFont()->setBold(TRUE);
$sheet->mergeCells('C6:I6');

$sheet->setCellValue( 'C7', 'NUMERIC DISTRIBUTION %' );
$sheet->getStyle( 'C7')->getFont()->setBold(TRUE);
$sheet->mergeCells('C7:I7');
*/




// Write MATRIX Headers Rows
echo "\n ".date('Y-m-d H:i:s')." | Write MATRIX Headers Rows ";


    // PROVINCE - STORE
    $sheet->setCellValue( 'A'.$ndgap_table_start_row , 'PROVINCE');
    $sheet->getColumnDimension( 'A'.$ndgap_table_start_row )->setAutoSize(TRUE);
    $sheet->getStyle( 'A'.$ndgap_table_start_row)->getFont()->setBold(TRUE);
    $sheet->getStyle('A'.$ndgap_table_start_row)->getAlignment()->setVertical('bottom');
    $sheet->getStyle('A'.$ndgap_table_start_row)->applyFromArray($styleMatrixBordersArray);  
    $sheet->setCellValue( 'B'.$ndgap_table_start_row , 'STORE NAME'); 
    $sheet->getStyle( 'B'.$ndgap_table_start_row )->getFont()->setBold(TRUE);
    $sheet->getStyle('B'.$ndgap_table_start_row)->getAlignment()->setVertical('bottom');
    $sheet->getStyle('B'.$ndgap_table_start_row)->applyFromArray($styleMatrixBordersArray);  
    //$sheet->getColumnDimension( 'B'.$ndgap_table_start_row )->setAutoSize(FALSE);
    //$sheet->getColumnDimension( 'B'.$ndgap_table_start_row )->setWidth(200);

    $sheet->setAutoFilter('A'.$ndgap_table_start_row.':'.'B'.$ndgap_table_start_row); // AUTOFILTER on PROV AND STORE


    // Write "Listed Articles" Header
echo "\n ".date('Y-m-d H:i:s')." | Write Listed Articles Headers ";


    
    $sheet->setCellValue( $ndgap_table_start_col . $ndgap_table_start_row, 'LISTED ARTICLES IN STORE');        
    $sheet->getStyle($ndgap_table_start_col . $ndgap_table_start_row)->getAlignment()->setTextRotation(90);
    $sheet->getColumnDimension($ndgap_table_start_col)->setWidth(4);
    $sheet->getStyle($ndgap_table_start_col . $ndgap_table_start_row)->getAlignment()->setHorizontal('center');
    $sheet->getStyle($ndgap_table_start_col . $ndgap_table_start_row)->getAlignment()->setVertical('bottom');
    $sheet->getStyle($ndgap_table_start_col . $ndgap_table_start_row)->getFont()->setBold(TRUE);
    $sheet->getStyle($ndgap_table_start_col . $ndgap_table_start_row)->getFont()->setSize(8);
    //$sheet->getColumnDimension($ndgap_table_start_col)->setAutoSize(TRUE);
    $sheet->getStyle($ndgap_table_start_col . $ndgap_table_start_row)->applyFromArray($styleMatrixBordersArray);
    $ndgap_table_start_col++; 
    
    $articleHeaderRow = $ndgap_table_start_row;
    
    // Write Article Name Headers
echo "\n<br /> date('Y-m-d H:i:s') - Write Article Names Headers ";
flush();
ob_flush();

    foreach($articlesID_arr as $a) {

        $sheet->setCellValue( $ndgap_table_start_col . $ndgap_table_start_row, $articles_ref_arr[$a]);        
        $sheet->getStyle($ndgap_table_start_col . $ndgap_table_start_row)->getAlignment()->setTextRotation(90);
        $sheet->getColumnDimension($ndgap_table_start_col)->setWidth(4);
        $sheet->getStyle($ndgap_table_start_col . $ndgap_table_start_row)->getAlignment()->setHorizontal('center');
        $sheet->getStyle($ndgap_table_start_col . $ndgap_table_start_row)->getAlignment()->setVertical('bottom');
        //$sheet->getStyle($ndgap_table_start_col . $ndgap_table_start_row)->getFont()->setBold(TRUE);
        $sheet->getStyle($ndgap_table_start_col . $ndgap_table_start_row)->getFont()->setSize(8);
        //$sheet->getColumnDimension($ndgap_table_start_col)->setAutoSize(TRUE);
        $sheet->getStyle($ndgap_table_start_col . $ndgap_table_start_row)->applyFromArray($styleMatrixBordersArray);

        //$sheet->getComment($ndgap_table_start_col . $ndgap_table_start_row)->setAuthor('MarketForce');
        $sheet->getComment($ndgap_table_start_col . $ndgap_table_start_row)->setWidth("400px");
        $sheet->getComment($ndgap_table_start_col . $ndgap_table_start_row)->setheight("15px");
        $sheet->getComment($ndgap_table_start_col . $ndgap_table_start_row)->getText()->createTextRun($articles_ref_arr[$a]);                                                           

       
        $ndgap_table_start_col++;

        if($ndgap_table_start_col >= $max_col_num_matrix ) {
            $max_col_num_matrix = $ndgap_table_start_col;
        }

    }



// Generate Matrix Array (PIVOT)
$lpcnt = 0;

echo "\n\n<br /><br />". date('Y-m-d H:i:s') ."- Processing Data\n<br />";
flush();
ob_flush();


echo "\n\n<br /><br />". date('Y-m-d H:i:s') ."- Generate Matrix Array (PIVOT)\n<br />";
flush();
ob_flush();

foreach($data_array['STOREARTICLEMATRIX'] as $r) {

                    
    $lpcnt++;
    
    echo ".";


   


    $obj_days_since_last_sold  = date_diff(date_create($r['Last_Sold']), date_create($data_array['INFO']['DataDate']));
    $days_since_last_sold      =  $obj_days_since_last_sold->format("%a");

    $obj_days_since_last_received  = date_diff(date_create($r['Last_Received']), date_create($data_array['INFO']['DataDate']));
    $days_since_last_received      =  $obj_days_since_last_received->format("%a");

    $obj_days_since_last_ordered  = date_diff(date_create($r['Last_Ordered']), date_create($data_array['INFO']['DataDate']));
    $days_since_last_ordered      = $obj_days_since_last_ordered->format("%a");


    $matrix[$r['Store_Province']][$r['Store_Name']][] = array(  'Article_Number' => $r['Store_Article_Number'],
                                                                'Article_Desc'   => $r['Store_Article_Desc'],
                                                                'Article_Status' => $r['Store_Article_Status'],
                                                                'Status_Description' => $r['Status_Description'],
                                                                'Soh_Qty'        => $r['SOH_QTY'],
                                                                'Days_Cover'     => $r['Days_Cover'],
                                                                'Last_Sold'      => $r['Last_Sold'],
                                                                'Last_Received'  => $r['Last_Received'],
                                                                'Last_Ordered'   => $r['Last_Ordered'],
                                                                'DaysSince_LastSold'     => $days_since_last_sold,
                                                                'DaysSince_LastReceived' => $days_since_last_received,
                                                                'DaysSince_LastOrdered' => $days_since_last_ordered
                                                            );

    

    

}; // END FOR LOOP


// Generate Matrix
//echo "\n<br />";

$row_num = $ndgap_table_start_row + 1;

$lpcnt2 = 0;
foreach($matrix as $mk => $mv) {   
            $article_found = 0; 

            $lpcnt2++;
    
            echo "#";

            if($lpcnt >= 13) {
                $lpcnt2 = 0;
                echo "\n <br />";
            };

            
            foreach($mv as $sk => $sv) {


                    // GET STORE ID
                    // HACK - Get store Id out of Name.... Eish.- @TODO determine if better fix. e.g. Do we put StoreID in every data se in this array? seems wasteful
                    // Look at storeID/Name map as a better option.
                    

                    //echo "<br />GET STORE ID: ";
                preg_match_all("/\[([^\]]*)\]/", $sk, $store_id_arr); 
                $store_id = $store_id_arr[1][0];


                echo "\n\n Store_id:" . $store_id ."\n";

                $col_num = 'A';
                $sheet->setCellValue( $col_num . $row_num, $mk ); // Province
                $sheet->getStyle($col_num . $row_num)->applyFromArray($styleMatrixBordersArray);
                $sheet->getStyle($col_num . $row_num)->getFont()->setSize(10);
                
                $col_num = 'B';
                $sheet->setCellValue( $col_num . $row_num, $sk ); // Store
                $sheet->getStyle($col_num . $row_num)->applyFromArray($styleMatrixBordersArray);
                $sheet->getStyle($col_num . $row_num)->getFont()->setSize(10);



                $col_num = 'C';
                // Articles Listed per Store count
               // Set 0 Count on stores with no listed articles
                if(!$data_array['ARTICLESPERSTORE'][$store_id]) {
                        $data_array['ARTICLESPERSTORE'][$store_id] = 0;

                        // SET TO BOLD AND RED
                        $sheet->getStyle($col_num . $row_num)->getFont()->setBold(TRUE);
                        $sheet->getStyle($col_num . $row_num)->getFont()->getColor()->setARGB('FFEE0000');

                        // HIGHLIGHT STORE (visibility)
                        $sheet->getStyle('B' . $row_num)->getFont()->setBold(TRUE);
                        $sheet->getStyle('B' . $row_num)->getFont()->getColor()->setARGB('FFEE0000');


                };

                $sheet->setCellValue( $col_num . $row_num, $data_array['ARTICLESPERSTORE'][$store_id] ); // Count
                $sheet->getStyle($col_num . $row_num)->applyFromArray($styleMatrixBordersArray);
                $sheet->getStyle($col_num . $row_num)->getFont()->setSize(10); 
                
                $lprowcnt = 0;
                foreach($articlesID_arr as $a) {

                   echo ' '. $a . ' | ';

                        $col_num++;
                    
                        // Set Default
                                                // NOT In Store
                        // Colour CELL
                        $sheet->getStyle($col_num . $row_num)->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('FFFF0000'); 
                        $sheet->getStyle($col_num . $row_num)->getFont()->getColor()->setARGB('FFFF0000'); 
                        
                        foreach($sv as $listed_articles) {
                            
                            //$article_status = 
                            if($listed_articles['Article_Number'] == $a) {                                
                                //$article_found = 1;
                            echo "\n ARTICLE FOUND: " . $listed_articles['Article_Desc'];

                                    // Set Cell COlour based on Status
                                // 01 - 05  / 03 (ACTIVE)
                                if(in_array($listed_articles['Article_Status'], $status_active_arr )) {
                                    $sheet->getStyle($col_num . $row_num)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FF00EE00'); 
                                    $sheet->getStyle($col_num . $row_num)->getFont()->getColor()->setARGB('FF00EE00');
                                    $cnt_active++;
                                
                                    
                                } else {
                                    // Non-Active Status
                                    // Set Cell Colour for "TEMP" Status Codes.
                                    $cnt_inactive++;
                                    if(in_array($listed_articles['Article_Status'], $status_temp_arr))  {  
                                                                        
                                        $sheet->getStyle($col_num . $row_num)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFEABC07');
                                        $sheet->getStyle($col_num . $row_num)->getFont()->getColor()->setARGB('FF000000');
                                        // TEMP HACK for "The John DAYS COVER Error"
                                        $listed_articles['Days_Cover'] = '-';
                                    } else {
                                        $sheet->getStyle($col_num . $row_num)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFAA0000');
                                        $sheet->getStyle($col_num . $row_num)->getFont()->getColor()->setARGB('FF000000');
                                    }
                                    $sheet->setCellValue( $col_num . $row_num, $listed_articles['Article_Status']);
                                    $sheet->getStyle($col_num . $row_num)->getAlignment()->setHorizontal('center');                                    
                                }


                                // Set status code as cell text
                                //$sheet->setCellValue( $col_num . $row_num, $listed_articles['Article_Status']);

                                // Add Comment 
                                $comment  = $listed_articles['Article_Desc'] . "\r\n";
                                $comment .= $listed_articles['Article_Number'] . "\r\n";
                                $comment .= 'Status: '.$listed_articles['Article_Status'].' - '.  $listed_articles['Status_Description'] . "\r\n";
                                $comment .= 'SOH: ' . $listed_articles['Soh_Qty'] . "\r\n";
                                $comment .= 'Cover: ' . $listed_articles['Days_Cover'] . "\r\n";
                                $comment .= 'Sold: ' . $listed_articles['Last_Sold'] . ' ['.$listed_articles['DaysSince_LastSold'].' Day(s)]' . "\r\n";
                                $comment .= 'Rec: ' . $listed_articles['Last_Received'] . ' ['.$listed_articles['DaysSince_LastReceived'].' Day(s)]' . "\r\n";
                                $comment .= 'Ord: ' . $listed_articles['Last_Ordered'] . '   ['.$listed_articles['DaysSince_LastOrdered'].' Day(s)]' . "\r\n";


                                                                                   
                                

                                $sheet->getComment($col_num . $row_num)->setAuthor('MarketForce');
                                $sheet->getComment($col_num . $row_num)->setWidth("400px");
                                $sheet->getComment($col_num . $row_num)->setheight("170px");
                                $sheet->getComment($col_num . $row_num)->getText()->createTextRun($comment);                                                           

                                $comment ='';
                                unset($comment); 


                               
                                                        
                            }
                        
                        }

                        
                        
                        $sheet->getStyle($col_num . $row_num)->getFont()->setSize(10);
                        

                        $comment = '';
                        unset($comment);
                        $sheet->getStyle($col_num . $row_num)->applyFromArray($styleMatrixBordersArray);                
                }
                
                $row_num++;
                if($row_num >= $max_row_num_matrix) {
                    $max_row_num_matrix = $row_num;
                } 

                
            };
};

// ADD TOTAL STORES LISTED FOR ARTICLE SUM  (BOTTOM, TOP)
//$row_num++;  // NOt required
// ADD ROW "title" - LISTED STORES (count)

echo "\n\n". date('Y-m-d H:i:s')." | Add Total Counts Column/Row Title";


//$ndgap_table_start_row
$listedStoresTopRow = $ndgap_table_start_row - 1;

$sheet->mergeCells('A'.$row_num.':C'.$row_num); 
$sheet->setCellValue( 'A' . $row_num, 'STORES WITH ARTICLE LISTED' ); //Counts Totals - Bottom
$sheet->getStyle('A'.$row_num.':C'.$row_num)->applyFromArray($styleMatrixBordersArray);
$sheet->getStyle('A'.$row_num)->getAlignment()->setHorizontal('right');
$sheet->getStyle('A'.$row_num)->getFont()->setBold(TRUE);
$sheet->getStyle('A'.$row_num)->getFont()->setSize(9);

       


$sheet->mergeCells('A'.$listedStoresTopRow.':C'.$listedStoresTopRow); 
$sheet->setCellValue( 'A' . $listedStoresTopRow, 'STORES WITH ARTICLE LISTED' ); //Counts Totals - TOP
$sheet->getStyle('A' . $listedStoresTopRow.':C'.$listedStoresTopRow)->applyFromArray($styleMatrixBordersArray);
$sheet->getStyle('A'.$listedStoresTopRow)->getAlignment()->setHorizontal('right');
$sheet->getStyle('A'.$listedStoresTopRow)->getFont()->setBold(TRUE);
$sheet->getStyle('A'.$listedStoresTopRow)->getFont()->setSize(9);


echo "\n\n". date('Y-m-d H:i:s')." | Add Total Counts Values";




$col_num = 'D';
foreach($articlesID_arr as $a) {                 

    // $data_array['STORESPERARTICLE'][$a]
    // Catch and set no value.
    if(empty($data_array['STORESPERARTICLE'][$a])) {
        $data_array['STORESPERARTICLE'][$a] = 0;
        $sheet->getStyle($col_num . $row_num)->getFont()->setBold(TRUE);
        $sheet->getStyle($col_num . $row_num)->getFont()->getColor()->setARGB('FFEE0000');

        $sheet->getStyle($col_num . $listedStoresTopRow)->getFont()->setBold(TRUE);
        $sheet->getStyle($col_num . $listedStoresTopRow)->getFont()->getColor()->setARGB('FFEE0000');

         // HIGHLIGHT ARTICLE HEADER (visibility)
         
         $sheet->getStyle($col_num . $articleHeaderRow)->getFont()->setBold(TRUE);
         $sheet->getStyle($col_num . $articleHeaderRow)->getFont()->getColor()->setARGB('FFEE0000');

    }

    $sheet->setCellValue( $col_num . $row_num, $data_array['STORESPERARTICLE'][$a]); // Bottom 
    $sheet->getStyle($col_num . $row_num)->applyFromArray($styleMatrixBordersArray);
    $sheet->getStyle($col_num. $row_num)->getAlignment()->setHorizontal('center'); 
    $sheet->getStyle($col_num. $row_num)->getFont()->setSize(9);  
    
    
    $sheet->setCellValue( $col_num . $listedStoresTopRow, $data_array['STORESPERARTICLE'][$a]); // TOP
    $sheet->getStyle($col_num . $listedStoresTopRow)->applyFromArray($styleMatrixBordersArray);
    $sheet->getStyle($col_num.$listedStoresTopRow)->getAlignment()->setHorizontal('center'); 
    $sheet->getStyle($col_num.$listedStoresTopRow)->getFont()->setSize(9);

    
    $col_num++;

}



$sheet->getSheetView()->setZoomScale(85);

echo "\MEM USAGE: " . convert(memory_get_usage(true)); 
$sheet->garbageCollect();
echo "\MEM USAGE [POST sheet->GarbageCollect] " . convert(memory_get_usage(true)); 


$row        = null;
$data_json  = null;
$data_array = null;
$col_num    = null;
$articlesID_arr = null;
$mv         = null;
$matrix     = null;
$storesID_arr   = null;
$stores_arr     = null;
$stores_count   = null;
$articlesID_tmp_arr = null;    
$articlesID_arr = null;
$articles_count = null;
$articles_ref_arr = null;

unset($row);
unset($data_json); 
unset($data_array); 
unset($col_num ); 
unset($articlesID_arr); 
unset($mv); 
unset($matrix ); 
unset($storesID_arr); 
unset($stores_arr); 
unset($stores_count); 
unset($articlesID_tmp_arr);    
unset($articlesID_arr); 
unset($articles_count); 
unset($articles_ref_arr); 

echo "\MEM USAGE [POST VAR to NULL]: " . convert(memory_get_usage(true)); 

gc_collect_cycles();

echo "\MEM USAGE [POST PHP gc_collect_cycles]: " . convert(memory_get_usage(true)); 
echo "\n#MEM PEAK: ".floor((memory_get_peak_usage())/1024/1024)."MB"."\n";

$numdist_end_time = microtime(true);
$numdist_time =  $numdist_end_time - $numdist_start_time;





