<?php


/**
 * 
 *  $a['client_id']. ' ' .$channel.' ' .$data_date .' '. $r
 * 
 * 
 */

echo "\n*********** REPORT SHEET GENERATOR ******************\n";

// Includes
$start_time = microtime(true); 


error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 1);
set_time_limit(0);  
ini_set('memory_limit', -1); 

include '../includes/db.inc.php';
include '../includes/function.inc.php';
include '../includes/general.inc.php';
include '../includes/function.generatorV2.inc.php';

require '../vendor/autoload.php';

/** Inputs */
$client_id = $argv[1];
$channel   = $argv[2];
$data_date = $argv[3];
$report    = $argv[4];


$datetime = date('Y-m-d H:i'); // General create/base/today date

echo "\nRECEIVED CLI PARAMS: \n";
print_r($argv);

/** Check Input count */
if($argc != 5) {
    echo "\nERROR: Invalid paramters";
    echo "\nREQUIRED: ClientID Channel dataDate ReportName";
    echo "\nPASSED: $client_id $channel $data_date $report";
    exit(1);
}



/** Load Client INFO */
$client_info_json = file_get_contents("https://marketforce.co.za/qsapi/ClientInfo.php?cid=".$client_id."&channel=PNP&format=JSON");

$client_info_arr = json_decode($client_info_json, TRUE);
$client_info     = $client_info_arr['CLIENTINFO'];

echo "\n** DEBUG: Client Info";
print_r($client_info);


/** Load PHPSpreadSheets Lib  **/

/*
use PhpOffice\PhpSpreadsheet\Chart\Chart;
use PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use PhpOffice\PhpSpreadsheet\Chart\Layout;
use PhpOffice\PhpSpreadsheet\Chart\Legend;
use PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use PhpOffice\PhpSpreadsheet\Chart\Title;
*/

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
$spreadsheet->getProperties()
    ->setCreator("MarketForce")
    ->setLastModifiedBy("sipho")
    ->setTitle("ChannelView PNPStatus Report")
    ->setSubject("Channel View PNP Report")
    ->setDescription("Alpha 0.9 - Report On Supplier Stores and Article Listing status, Orders and fufillment, Logistic and Stock Supply")
    ->setKeywords("Report PNP Articles SKU Stores Orders gap stock channel")
    ->setCategory("Report - PNP");

\PhpOffice\PhpSpreadsheet\Shared\StringHelper::setDecimalSeparator('.');
\PhpOffice\PhpSpreadsheet\Shared\StringHelper::setThousandsSeparator(' ');


echo runGeneratorProcesses($spreadsheet, $client_info, $report, $data_date, $channel, $mysqli);
echo "\n";
$gc_collected = gc_collect_cycles();
echo "GENERATOR - GC GARBAGE COLLECTION: " .  $gc_collected;
